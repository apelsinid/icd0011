package orders;

import model.Order;
import model.OrderRow;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class OrderController {
    private OrderRepository repository;
    private OrderService orderService;

    public OrderController(
            OrderService orderService,
            OrderRepository repository) {
        this.repository = repository;
        this.orderService = orderService;
    }
    
    @GetMapping("orders")
    public List<Order> getAllOrders() {
        return repository.getOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable Long id) {
        return repository.getOrderById(id);
    }

    @GetMapping("orders/{id}/installments")
    public List<OrderInstallmentDto> getOrderInstallments(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end,
            @PathVariable Long id) {
        return orderService.generateOrderInstallments(id, start, end);
    }

    @PostMapping("orders")
    public OrderDto createOrder(@RequestBody @Valid OrderDto incoming) {
        Order order = new Order();
        order.setOrderNumber(incoming.getOrderNumber());
        List<OrderRow> orderRows = incoming.getOrderRows().stream()
                .map(row -> new OrderRow(
                        row.getItemName(),
                        row.getQuantity(),
                        row.getPrice()))
                .collect(Collectors.toList());
        order.setOrderRows(orderRows);
        repository.addOrder(order);
        OrderDto outbound = new OrderDto();
        outbound.setId(order.getId());
        outbound.setOrderNumber(order.getOrderNumber());
        outbound.setOrderRows(incoming.getOrderRows());
        return outbound;
    }

    @DeleteMapping("orders/{id}")
    public void deleteOrder(@PathVariable Long id) {
        repository.deleteOrder(id);
    }
}
