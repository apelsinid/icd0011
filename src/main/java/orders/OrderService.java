package orders;

import model.Order;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    private OrderRepository repository;

    public OrderService(OrderRepository repository) {
        this.repository = repository;
    }

    public List<OrderInstallmentDto> generateOrderInstallments(Long orderId, LocalDate start, LocalDate end) {
//        Jagatav summa on tellimuse ridade kogusumma (hind * kogus).
//                Osamakseteks jagamine toimub järgmiste reeglite alusel:
//        - osamaksed tulevad kuu 1. päevale;
//        - kui periood algab pärast 1. kuupäeva, siis on makse perioodi esimesel päeval;
//        - minimaalne ühik on 1 Euro; kui summa ei jagu Euro täpsusega,
//        siis jaotatakse vahe viimaste osamaksete vahel;
//        - miinimum osamakse summa on 3 Eurot.

        Order order = repository.getOrderById(orderId);
        List<OrderInstallmentDto> list = new ArrayList();

        var monthsBetween = ChronoUnit.MONTHS.between(
                start.withDayOfMonth(1),
                end.withDayOfMonth(1));

        var totalSum = order.totalSum();
        var months = monthsBetween + 1; // current month as well

        var singleInstallmentAmount = Math.floorDiv(totalSum, Math.toIntExact(months));

        while (months == 1 || singleInstallmentAmount < 3) {
            months--;
            singleInstallmentAmount = Math.floorDiv(totalSum, Math.toIntExact(months));
        }

        if (months == 1) {
            list.add(new OrderInstallmentDto(totalSum, start));
            return list;
        }

        var currentDate = start;
        for (int i = 1; i <= months; i++) {

            var installment = new OrderInstallmentDto(singleInstallmentAmount, currentDate);

            currentDate = nextInstallmentDate(currentDate); // get next

            if (i == months) {
                installment.amount = totalSum; // whats left from total
            }
            if (i == months - 1) {
                installment.amount = totalSum / 2; // divided
            }

            list.add(installment);

            totalSum -= installment.amount; // deduct from total
        }

        return list;
    }

    ;

    private LocalDate nextInstallmentDate(LocalDate currentDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(convertToDateViaSqlDate(currentDate));
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date nextMonthFirstDay = calendar.getTime();
        return convertToLocalDateViaMilisecond(nextMonthFirstDay);
    }

    public Date convertToDateViaSqlDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }

    public LocalDate convertToLocalDateViaMilisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

}
