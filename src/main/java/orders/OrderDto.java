package orders;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.OrderRow;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private Long id;
    private String orderNumber;

    private List<@Valid OrderRowDto> orderRows = new ArrayList<>();

    public void addOrderRow(OrderRowDto orderRow) {
        orderRows.add(orderRow);
    }

    public int totalSum() {
        return orderRows.stream()
                .map(row -> row.getQuantity() * row.getPrice())
                .mapToInt(i -> i.intValue())
                .sum();
    }
}

