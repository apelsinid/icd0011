package orders;

import model.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class OrderRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Order> getOrders() {
        return em.createQuery("select o from Order o")
                .getResultList();
    }

    public Order getOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class);

        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Transactional
    public Order addOrder(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }

    @Transactional
    public void deleteOrder(Long id) {
        Order order = getOrderById(id);
        em.remove(order);
    }

}
