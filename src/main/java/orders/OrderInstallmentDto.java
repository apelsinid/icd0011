package orders;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class OrderInstallmentDto {

    @NotNull
    @Range(min = 3)
    public int amount;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    public LocalDate date;

    public OrderInstallmentDto(int amount, LocalDate date) {
        this.amount = amount;
        this.date = date;
    }
}
