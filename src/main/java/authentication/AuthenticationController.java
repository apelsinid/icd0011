package authentication;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

    @GetMapping("version")
    public String getVersion() {
        return "1.0.0";
    }

    @GetMapping("users")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUsers() {
        return "users";
    }

    @PreAuthorize("#userName == authentication.name or hasAuthority('ROLE_ADMIN')")
    @GetMapping("users/{userName}")
    public String getUserByName(@PathVariable String userName)
    {
        return userName;
    }

}
