package config;

import config.handlers.ApiAccessDeniedHandler;
import config.handlers.ApiEntryPoint;
import config.jwt.JwtAuthenticationFilter;
import config.jwt.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
@PropertySource("classpath:/application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

//    public SecurityConfig(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }

    @Value("${jwt.signing.key}")
    private String jwtKey;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests().antMatchers("/api/version").permitAll();
        http.authorizeRequests().antMatchers("/api/login").permitAll();

        http.authorizeRequests().antMatchers("/api/**").authenticated();

//
        http.exceptionHandling()
                .authenticationEntryPoint(new ApiEntryPoint());

        http.exceptionHandling()
                .accessDeniedHandler(new ApiAccessDeniedHandler());
//
        var apiLoginFilter = new JwtAuthenticationFilter(
                authenticationManager(), "/api/login", jwtKey);

        http.addFilterAfter(apiLoginFilter, LogoutFilter.class);

        var jwtAuthFilter = new JwtAuthorizationFilter(authenticationManager(), jwtKey);
        http.addFilterBefore(jwtAuthFilter, LogoutFilter.class);

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
//        builder.inMemoryAuthentication()
//                .passwordEncoder(new BCryptPasswordEncoder())
//                .withUser("user")
//                .password("$2a$10$aQQZ6tv56lnwTbTTNuoWuehiuXkIDuMibIxK0Ikkb6FkpLJGsP8cK")
//                .roles("USER")
//                .and()
//                .withUser("admin")
//                .password("$2a$10$ynkMH2763FkTlhDs8bwEo.8NKSlcZfl6YObZfLIcy9zYTgQPLwGnS")
//                .roles("USER", "ADMIN");

//        builder.jdbcAuthentication().dataSource(dataSource)
//                .usersByUsernameQuery(
//                        "select username, password, enabled from users where username=?")
//                .authoritiesByUsernameQuery(
//                        "select username, authority from authorities where username=?");
        builder.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder());
    }
}
