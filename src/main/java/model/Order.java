package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Order extends BaseEntity {
    private Long id;
    @Column(name = "order_number")
    private String orderNumber;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "order_rows",
            joinColumns=@JoinColumn(name = "orders_id",
                    referencedColumnName = "id")
    )
    private List<@Valid OrderRow> orderRows = new ArrayList<>();

    public void addOrderRow(OrderRow orderRow) {
        orderRows.add(orderRow);
    }

    public int totalSum() {
        return orderRows.stream()
                .map(row -> row.getQuantity() * row.getPrice())
                .mapToInt(i -> i.intValue())
                .sum();
    }
}
