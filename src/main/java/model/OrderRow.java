package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NonNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "order_rows")
public class OrderRow {
    @Column(name = "item_name")
    private String itemName;

    @NonNull
    private Integer quantity;

    @NonNull
    private Integer price;

    public OrderRow() {
    }

    public OrderRow(String itemName, Integer quantity, Integer price) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
