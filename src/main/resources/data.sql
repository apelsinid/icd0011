
insert into orders (order_number) values
('asdasdasd'),
('asdasd'),
('12dsadasdasd');

insert into order_rows (orders_id, item_name, quantity, price) values
(1, 'tellimus', 10, 120),
(1, 'tellimus', 10, 120),
(2, 'tellimus', 300, 5),
(2, 'tellimus', 300, 5),
(2, 'tellimus', 300, 5),
(3, 'tellimus', 20, 10);

insert into USERS (username, password, enabled, first_name) values
('user', '$2a$10$MPVfioWpSBxD9VNO/SqTp.e7aKNAMrhfvo6Tq1PTXMy16iT5jlj.C', true, 'eesnimi'),
('admin', '$2a$10$HqTfKtDJzMLs9p1JoO/TrOHOdUvVbn.cFWj3Nax6TZLxKq3fMvWeu', true, 'Admini eesnimi');

insert into AUTHORITIES (username, authority) values
('user', 'ROLE_USER'),
('admin', 'ROLE_USER'),
('admin', 'ROLE_ADMIN');