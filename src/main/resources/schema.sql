DROP TABLE IF EXISTS order_rows;
DROP TABLE IF EXISTS orders;
DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 AS INTEGER START WITH 1;

CREATE TABLE orders (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
  order_number VARCHAR(255)
);

CREATE TABLE order_rows (
  item_name VARCHAR(255),
  price INT,
  quantity INT,
  orders_id BIGINT,
  FOREIGN KEY (orders_id)
    REFERENCES orders ON DELETE CASCADE
);

DROP TABLE IF EXISTS AUTHORITIES;
DROP TABLE IF EXISTS USERS;

CREATE TABLE USERS (
    username VARCHAR(255) NOT NULL PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    enabled BOOLEAN NOT NULL,
    first_name VARCHAR(255) NOT NULL
);

CREATE TABLE AUTHORITIES (
     username VARCHAR(50) NOT NULL,
     authority VARCHAR(50) NOT NULL,
     FOREIGN KEY (username) REFERENCES USERS
         ON DELETE CASCADE
);
-- drop table IF EXISTS order_rows;
-- drop table IF EXISTS orders;
--
-- drop sequence IF EXISTS seq1;
-- drop sequence IF EXISTS seq2;
--
-- create sequence seq1 start with 1;
-- create sequence seq2 start with 1;
--
-- create TABLE orders (
--     id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
--     order_number varchar
-- );
--
-- create TABLE order_rows (
--     id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq2'),
--     item_name varchar,
--     quantity int,
--     price int,
--     order_id BIGINT not null,
--     CONSTRAINT fk_orders
--         FOREIGN KEY(order_id)
-- 	        REFERENCES orders(id)
-- 	        ON delete CASCADE
-- );
